<?php

class SearchController extends BaseController {

	public function SearchCompany()
	{
	$seatleasemin = DB::table('SeatModel_Comp')
							->select('priceRangeMin')
							->whereRaw('seatModelID = 1')
							->whereRaw('companyID = companylist.ID')->toSql();
	
	
	$fullmanagemin = DB::table('SeatModel_Comp')
							->select('pricerangeMin')
							->whereRaw('seatmodelID = 2')
							->whereRaw('companyID = companylist.ID')->toSql();
	
	$perpage = (Input::has('pageresult'))?Input::get('pageresult'):5;
	
	$searchtype = Input::get('searchtype');
	
	//$prioritylist = CompanyList::where("stickysearch",1)->orderby("ordersort")->spaginate(3);
	$prioritylist = PriorityList::with('companylist')->orderby('ordersort','ASC')->paginate(3);;
	
	if(Input::has('sort')){
			$ordering = explode('|',Input::get('sort'));
			if($ordering[0] == "company"){
				$order = $ordering[0]. "list.name";
			}
			else{
				$order = $ordering[0];
			}
			$sort = $ordering[1];
	}

	else{
		$order = "companylist.name";
		$sort = "ASC";
	}
	
		if($searchtype=="compname"){
			$search = Input::get('companyname');
			$result = DB::select(DB::raw("Select * from companylist WHERE name = '$search'"));
			if(count($result) == 1){
				return Redirect::to('company/' . $result[0]->id);			
			}
		}
		
		elseif($searchtype=="comploc"){
			
			/*
			$result = CompanyList::with(array('seatmodel','location'),function($query)
			{
				$search = Input::get('location');
				$query->where('locaton.id',$search);
			})->get();
			*/
			
			$result = CompanyList::whereHas('location',function($q){
				$search = Input::get('location');
				$q->where("location.id",$search);
			})->orderby($order,$sort)->get();
			
				//echo "<pre>";print_r($result);exit;
			
			
		}
		
		elseif($searchtype=="quicksearch"){
			
	
	
			
			$result = CompanyList::with('seatmodel')
					->select(array('companylist.*',DB::raw("({$seatleasemin}) as seatleasemin, ({$fullmanagemin}) as fullmanagemin")))
			
			->whereHas('seatmodel',function($q){
				if(Input::has('price')){
					$pricemax = (Input::get('price') !== null)?max(Input::get('price')):0;
					$q->whereIn("priceRangeMin",Input::get('price'));
				}
			})
			
			->whereHas('location', function($q){
				$search = Input::get('qloc',array());
				if(Input::has('qloc')){
					$q->whereIn("location.id",$search);
				}
			})
			
						
			->orderby($order,$sort)->paginate($perpage);
			
			
			
			//echo "<pre>";print_r($result);exit;
			
			
			
		}
		
		
		
		elseif($searchtype=="advsearch"){
		
			$aumanaged = Input::get('fullmanaged');
			$operator = (Input::get('fullmanaged') == -1)?">=":"=";

			$result = CompanyList::where('aumanaged',$operator,$aumanaged)
			->select(array('companylist.*',DB::raw("({$seatleasemin}) as seatleasemin, ({$fullmanagemin}) as fullmanagemin")))
			->whereHas('services',function($query){
				if(Input::has('service')){
					$query->whereIn("services.id",Input::get('service'));
				}
			})
			->whereHas('seatmodel',function($query){
				if(Input::has('seatmodel')){
					$strseatmodel = (Input::get('seatmodel') == 0)?array(1,2):Input::get('seatmodel');
					$query->whereIn("SeatModel.id",Input::get('seatmodel'));
					
				if(Input::has('priceadv')){
					$query->whereIn("priceRangeMin",Input::get('priceadv'));
				}
					
				}
			})
			//->whereHas('seatmodel',function($query){
			//	if(Input::has('budgetrange')){
			//		$query->where("priceRangeMin","=",Input::get('budgetrange'));
			//	}
			//})
			
			->whereHas('location',function($query){
				if(Input::has('advloc')){
					$query->whereIn("location.id",Input::get('advloc'));
					}
			})
			->whereHas('feature',function($query){
				if(Input::has('feature')){
					$query->whereIn("feature.id",Input::get('feature'));
					}
			})
			
			/*
			$result = CompanyList::whereHas('location',function($query){
				if(Input::has('advloc')){
					$query->whereIn("location.id",Input::get('advloc'));
				}
			})*/			
			->orderBy($order,$sort)->paginate($perpage);
			
			//exit;
		}
		
		$option = "";
		
		if($result->count() >= 1){
		$total = $result->getTotal();
		$totalpages = $result->getLastPage();
		$i = 1;
		
		$option = "";
		for($i;$i<=$total;$i+=5){
			$x = $i + 4;
			$page = $x;
			if($total <= $x){
				$x = "All";
			}
			//$option .= "<option value='?page=" . $page ."'>" . $i . "-" . $x . "</option>";
			$option[$page] = $x;
			//$page++;
		}
		}
		
		$query = array_except(Input::all(),array('sort','page'));
		$queryfull = array_except(Input::all(),array('view'));
		
		$querypage = array_except(Input::all(),array('pageresult','page'));
		$page_searches = array_except(Input::all(),array('page'));
		
		$stringfull = http_build_query($queryfull);
		$string = http_build_query($query);
		$stringpage = http_build_query($querypage);
		
		//$page
		
		$activetab = ($searchtype=="advsearch")?"#tab2":"#tab1";
		
		$searchLink = http_build_query( $query ) . $activetab;
		
		//echo "<pre>";print_r($result->toArray());exit;
		
		//echo(count($prioritylist));exit;
		
		return View::make('search')->with("search",$result)
									->with("query",$string)
									->with("queryfull",$stringfull)
									->with("searchLink",$searchLink)
									->with("activetab",$activetab)
									->with("option",$option)
									->with("querypage",$stringpage)
									->with("page_searches",$page_searches)
									->with("prioritylist",$prioritylist);
	
	}

	
	
	public function ViewLocation($id){
		$location = Location::find($id);
		
		$views = $location->clickviews + 1;
		$location->clickviews = $views;
		$location->save();
		
		//$location->increment('clickviews');
		$toplocation = DB::table('location')->where("isparent","!=",0)->select('id','name','clickviews')
			->orderby('clickviews','DESC')->take(10)->get();
		//	print_r($toplocation);
		
		$topbpo = CompanyList::whereHas('location',function($q) use ($location){
				$q->where("location.id",$location->id);

			})->orderby('pageview','DESC')->take(10)->get();
		
		//echo count($topbpo);
		
		return View::make('location')->with('location',$location)
									->with('toplocation',$toplocation)
									->with('topbpo',$topbpo);
	}
	
	public function ViewCompany($id){
	
	$company = CompanyList::find($id);
	
	$views = $company->pageview + 1;
	$company->pageview = $views;
	$company->save();
	
	return View::make('company')->with('company',$company);
	
	}
}
