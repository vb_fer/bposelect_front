<?php

class SeatModel extends Eloquent {

	protected $table = 'SeatModel';


	public function companylist()
		{
			return $this->belongsToMany('CompanyList','SeatModel_Comp','seatModelID','companyID')
			->withPivot('priceRangeMin','priceRangeMax')
			->orderBy('pivot_priceRangeMax','DESC');
		}
		
/*
	public function seat_package()
		{
			return $this->belongsToMany('SeatPackage','seatmodel_package','seatModelID','PackageID');
		}
	
	*/
	public function seatmodel_package()
		{
			//return $this->belongsToMany('SeatModelComp','SeatModel_Package','seatModelID','PackageID');
		}
	
}
