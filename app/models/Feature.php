<?php

class Feature extends Eloquent {

	protected $table = 'feature';
	
	public function companylist()
		{
			return $this->belongsToMany('CompanyList','feature_company','featureID','companyID');
		}
}

