<?php

class PriorityList extends Eloquent {

	protected $table = 'prioritylist';
	
	public function companylist(){
		return $this->belongsTo('CompanyList','companyID');
	}
	}