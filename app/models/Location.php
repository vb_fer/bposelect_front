<?php

class Location extends Eloquent {

	protected $table = 'location';
	
	public function companylist()
		{
			return $this->belongsToMany('CompanyList','location_company','locationID','companyID');
		}
}

