<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Route::get('/', function(){	return View::make('search'); });

Route::any('/',['uses'=>'SearchController@SearchCompany','as'=>'search']);

//Route::post('/','SearchController@SearchCompany');

//Route::get('/{searchtype}/{basic}',['uses'=>'SearchController@SearchCompany','as'=>'search']);

Route::get('company/{id}',['uses'=>'SearchController@ViewCompany']);
Route::get('location/{id}',['uses'=>'SearchController@ViewLocation']);

Route::get('banner/{id}',['uses'=>'HomeController@bannerRedirect']);

//Route::get();