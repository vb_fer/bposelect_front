<?php get_header(); 
//echo "<pre>"; print_r($search);
?>
<link rel="stylesheet" href={{URL::asset('assets/css/custom.css')}}>

<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
		<div class="innerPageHeader" style="margin-bottom:20px">
                        </div>
		<div class="innerPageContainer">
		<div class="searchContainer">
		@if($search->isEmpty())
		<div style="height:330px;text-align:center;line-height:235px;font-size:25px;">NO MATCHES FOR YOUR SEARCH</div>
		@else
		
		<div class="searchcontrols">
		<h3>Search Results</h3> 
		
		
		<div style="margin:10px 0px;">
		<div style="margin-right:20px;">Sort by: 
		
		{{Form::select('sort',array( 
			'company|asc'=>'Company (ascending)',
			'company|desc'=>'Company (descending)',
			'seatleasemin|ASC'=>'Seat Leasing (ascending)',
			'seatleasemin|DESC'=>'Seat Leasing (descending)',
			'fullmanagemin|ASC'=>'Fully Managed (ascending)',
			'fullmanagemin|DESC'=>'Fully Managed (descending)'
			),Input::get('sort'),array('id'=>'sortresult')) }}
		
		
		</div>
		<div class="listgrid">View as: 
		<a href="{{URL::to('/')}}?{{ $queryfull }}&view=list"><img src="<?php echo  get_template_directory_uri();?>/images/viewlist.png" /></a> | 
		<a href="{{URL::to('/')}}?{{ $queryfull }}&view=grid"><img src="<?php echo  get_template_directory_uri();?>/images/viewthumb.png" /></a>
		</div>
		<div>Show Results {{Form::select('pageselect',$option,Input::get('pageresult'),array('id'=>'pageresult')) }}</div>
		</div>
		<div class="paging">{{$search->appends($page_searches)->links()}}</div>
		</div>
		
		
		@if(Input::has('view'))
			
			@if(Input::get('view')=="grid")
				@include('gridview')
			
			@else
				@include('tableview')
			@endif
		@else
			@include('tableview')
		@endif
			<div class="paging2">{{$search->appends($page_searches)->links()}}</div>
			<div style="width:60%;float:right;margin-bottom:25px;clear:both">
			<p style="text-align:right">asterisk(*):This price is based on a monthly service fee per seat not including salary</br>
			Salary is based on industry and location</p>
			
			<p style="text-align:right">when a gray box is greyed out, this indicates that the option is out of budget range<br>
			or the field has not been selected within the advanced search criteria
			</p>
			
			</div>
			@endif
			<div style="width:60%;float:right;margin-bottom:25px;clear:both">
			<button onclick="window.location.href='http://{{$_SERVER['SERVER_NAME']}}/{{$activetab}}'" style="width:42%;display:inline">New Search</button> &nbsp; 
			<button onclick="window.location.href='http://{{$_SERVER['SERVER_NAME']}}/?{{$searchLink}}'" style="width:50%;display:inline;margin-right:0;float:right;">Modify Search</button>
			</div>
			</div>
			<div style="width:100%;height:120px;text-align:center;clear:both;">
			{{show_ads('search','bottom')}}
			</div>
			</div>
			
		</div>
</div>		
		
<?php get_footer(); ?>

<script type="text/javascript">
		$('#sortresult').change(function() {
			window.location =  "{{ URL::to('/')}}/? {{$query }}&sort=" + $(this).val();
		});
		
		$('#pageresult').change(function() {
			window.location =  "{{ URL::to('/')}}/? {{$querypage }}&pageresult=" + $(this).val();
		});
		
</script>
		
<script type="text/javascript">
$('ul.searchlist').each(function(){

  var LiN = $(this).find('li').length;

  if( LiN > 5){    
    $('li', this).eq(4).nextAll().hide().addClass('toggleable');
    $(this).append('<li class="more">Show all</li>');    
  }

});


$('ul.searchlist').on('click','.more', function(){

  if( $(this).hasClass('less') ){    
    $(this).text('Show all').removeClass('less');    
  }else{
    $(this).text('Show less').addClass('less'); 
  }

  $(this).siblings('li.toggleable').slideToggle();

});


</script>