<?php get_header(); ?>
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script>
function initialize() {
  var mapProp = {
    center:new google.maps.LatLng({{$location['gmapsLatitude']}}, {{$location['gmapsLongtitude']}}),
    zoom:13,
    mapTypeId:google.maps.MapTypeId.ROADMAP
  };
  var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>

<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
		<div class="innerPageHeader" style="margin-bottom:20px"></div>
		<div class="innerPageContainer">
		
		<div class="innerLeft" style = "padding:0 20px 20px 0;">
		<p style="font-size:35px;">Location : {{$location->name}}</p>
		
		
		@if($location->headerimage != "")
		<div style="margin:15px 0">
		<img src="{{Request::root()}}/banners/{{$location->headerimage}}" height="131px" width="693px" />
		</div>
		@endif
		
		<p>{{$location->locationdesc}}</p>
		
			<!-- start of ads placement -->
				<div style="width:100%;margin:auto;text-align:center;clear:both;">
				{{show_ads('location','mid')}}
				</div>
				
				<!--end ads placement-->
		
		<hr style="border-bottom:1px solid #ccc;margin:10px 0;width:98%"/>
		
		<p>Location Map</p>
		
		<div id="googleMap" style="width:98%;height:140px;"></div>
		
		<hr style="border-bottom:1px solid #ccc;margin:30px 0;width:98%"/>
		
		<div style="width:100%;">
		<div style="position:relative:display:inline;float:left;width:30%;border-right:1px solid #000;margin:5px;padding-left:20px;">
		Nearest Transport:
		<ul>
		<li>{{$location->nearesttransport}}</li>
		</ul>
		
		</div>
		
		<div style="position:relative:display:inline;float:left;width:30%;border-right:1px solid #000;margin:5px;padding-left:20px;">
		Distance to airport:
		<ul>
		<li>{{$location->distanceairport}}</li>
		</ul>
		
		</div>
		
		<div style="position:relative:display:inline;float:left;width:30%;margin:5px;padding-left:20px;">
		Top attractions in the area:
		
		<ul>
		<li>{{$location->topattractions}}</li>
		</ul>
		</div>
		
		</div>
		
		</div>
		
		
		<div class="innerRight">
		
		<div class="widget-area">
		<aside id="search-3" class="widget widget_search">
			<h3 class="widget-title">Got a BPO Company in Mind?</h3>
		<form action="/search/public/" method="Post" autocomplete="off" >
              
                      

                            <input type="hidden" name="searchtype" value="compname" />
							<input style="background-color:black;color:#fff" name="companyname" type="text" placeholder="Enter the company name" id="companyname" auto/>
                            
                        
						</form>
		</aside>
		
		</div>
		
		@if($location->galleryshortcode != '')
		<div class="widget-gallery" style="clear:both;margin:0 24px">
				<h4 class="widget-gallery-title">Image Gallery :</h4>
				
				
				{{ do_shortcode($location->galleryshortcode)}}
				
				
			</div>
			@endif
			
			<div class="widget-area">
			<aside id="search-3" class="widget widget_search">
				<h3 class="widget-title">Top Searched Location</h3>
				
				<ol style="padding-left:30px;color:#000">
				@foreach($toplocation as $toploc)
				
				<li style="list-style-type:decimal"><a href="{{URL::to('/')}}/location/{{$toploc->id}}">{{$toploc->name}}</a></li>
				
				@endforeach
				
				</ol>
				
			</aside>
			</div>
		
			<div class="widget-area">
			<aside id="search-3" class="widget widget_search">
				<h3 class="widget-title">Most Popular BPO in {{$location->name}}</h3>
				
				<ol style="padding-left:30px;color:#000">
				@foreach($topbpo as $topbpos)
				
				<li style="list-style-type:decimal"><a href="{{URL::to('/')}}/company/{{$topbpos->id}}">{{$topbpos->name}}</a></li>
				
				@endforeach
				
				
			</aside>
			</div>
		
		</div>
		
		<!-- start of ads placement -->
				<div style="width:100%;margin:20px 0;text-align:center;clear:both;">
				{{show_ads('location','bottom')}}
				</div>
				
				<!--end ads placement-->
		</div>
		
		


<?php get_footer(); ?>