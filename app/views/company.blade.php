<?php get_header(); ?>
<link rel="stylesheet" href="{{URL::asset('assets/css/bootstrap.css')}}"/>
<link rel="stylesheet" href="{{URL::asset('assets/css/custom.css')}}"/>
<script lang="text/javascript" src="{{URL::asset('assets/js/bootstrap.js')}}"></script>
<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
		<div class="innerPageHeader" style="margin-bottom:20px"></div>
		<div class="innerPageContainer">
		
		<div class="innerLeft">
		@if($company->bannerfilename != "")
		<img src="{{asset('banners/')}}/{{ $company->bannerfilename }}" width="696" />
		@endif
		
		<h2>{{$company->name}}</h2>
		
		<p>Company Description:</p>
		
		<p>{{$company->description}}</p>
		
		<p class="separator"></p>
		
		<div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs nav-justified" role="tablist">
    <li role="presentation" class="active"><a href="#contact" aria-controls="contact" role="tab" data-toggle="tab">Contact Information</a></li>
    <li role="presentation"><a href="#cost" aria-controls="Cost" role="tab" data-toggle="tab">Cost</a></li>
    <li role="presentation"><a href="#services" aria-controls="Services" role="tab" data-toggle="tab">Services</a></li>
    <li role="presentation"><a href="#features" aria-controls="Features" role="tab" data-toggle="tab">Features</a></li>
	<li role="presentation"><a href="#locations" aria-controls="Locations" role="tab" data-toggle="tab">Offshore Locations</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="contact">
	<b>Address:</b> {{ $company->headoffice}}<br>
	<b>Contact Number: </b>{{ $company->phone}} <br>
	<b>Website:</b> {{ $company->website}} <br>
	<b>Contact Person:</b> {{ $company->contactperson}}<br><br>
	<b>Follow us on:</b>
	</div>
    <div role="tabpanel" class="tab-pane" id="cost">
		
	<ul>
	@foreach($company->seatmodel as $seatmodel)
		<li> {{ $seatmodel->name }} : <b> $ {{ $seatmodel->pivot->priceRangeMin }} - {{ ($seatmodel->pivot->priceRangeMax == 0)?"Above": "$ " . $seatmodel->pivot->priceRangeMax }} </b></li>
		
			<ul class="list-column" style="margin-left:20px">
			@foreach($company->seatpackage as $seatpackage)
				
				@if($seatpackage->pivot->seatmodelID == $seatmodel->id)
					<li>{{$seatpackage->name}} </li>
				@endif
				
			@endforeach
			</ul>
	@endforeach
	</ul>
	
	</div>
    <div role="tabpanel" class="tab-pane" id="services">
	 <ul class="list-column">
	 @foreach($company->services as $service) 
		<li>{{ $service->name }}</li>
	@endforeach
	</ul>
	</div>
    <div role="tabpanel" class="tab-pane" id="features">
	<ul class="list-column">
	@foreach($company->feature as $feature)
		<li>{{ $feature->name }}</li>
	@endforeach
	</ul>
	</div>
	<div role="tabpanel" class="tab-pane" id="locations">
	<ul>
	@foreach($company->addresscompany as $address)
		
		<li><div style="margin-bottom:20px">
			<b>Address:</b> {{$address->address}}<br>
			<b>Contact Person:</b> {{$address->othercontactperson}}<br>
			<b>Contact Number:</b> {{$address->othercontactnumber}}
			</div>
		</li>
		
	@endforeach
	</ul>
	</div>
  </div>

</div>
		
		</div>
		
		<div class="innerRight">
		<div class="widget-area">
		<aside id="search-3" class="widget widget_search">
			<h3 class="widget-title">Got a BPO Company in Mind?</h3>
		<form action="/search/public/" method="Post" autocomplete="off" >
              
                 
                        

                            <input type="hidden" name="searchtype" value="compname" />
							<input style="background-color:black;color:#fff" name="companyname" type="text" placeholder="Enter the company name" id="companyname" auto/>
                            
                        
						</form>
		</aside>
		
		</div>
		@if($company->galleryshortcode != '')
			<div class="widget-gallery">
				<h4 class="widget-gallery-title">Image Gallery :</h4>
				{{ do_shortcode($company->galleryshortcode)}}
			</div>
		
		</div>
		@endif
		</div>
		</div>
</div>

		
<?php get_footer(); ?>