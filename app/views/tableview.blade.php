


<table id="searchTable" class="table table-striped table-hover table-bordered">

				<thead>
                    <tr>
                    
                      <th>Company Name</th>
                      <th>Primary Services <br>Options</th>
                      <th>Seat Leasing</th>
                      <th>Fully Manage Services</th>
                      <th>Location</th>
                      <th>Australian <br> managed</th>
                      <th>Additional Features</th>
					  
                    </tr>
                </thead>
				<tbody>
				<?php 
				$bgcolor="#fff"; 
				$seatmodel1 = "";
				$bgseatmodel1 = "";
				$ctr = 1;
				$ctr1 = $search->getFrom();
				$flag = ($prioritylist->getTotal() == 0 )?12:9;
				
				?>
				
				
				@foreach($prioritylist as $comp_priority)
					<tr style="background-color:#FFFFCD;">
					<td><a href='{{URL::to("company/$comp_priority->companyID")}}'>{{$comp_priority->companylist->name}}</a></td>
					<td>
					<ul class="searchlist">
					 @foreach ($comp_priority->companylist->services as $service) 
						<li>{{ $service->name }}</li>
					  @endforeach
					</ul>
					</td>
					<td>
					@foreach($comp_priority->companylist->seatmodel as $seatmodel)
					@if($seatmodel->id == 1)
											
						<b>${{ $seatmodel->pivot->priceRangeMin }} - ${{ ($seatmodel->pivot->priceRangeMax==0)?"up":$seatmodel->pivot->priceRangeMax }}  *</b>
						<ul class="searchlist">
						@foreach($comp_priority->companylist->seatpackage as $seatpackage)
							@if($seatpackage->pivot->seatmodelID == 1)
								<li>{{$seatpackage->name}}</li>
							@endif
						@endforeach
						</ul>
					@endif
					@endforeach
					</td>
					<td>
					@foreach($comp_priority->companylist->seatmodel as $seatmodel)
						@if($seatmodel->id == 2)
							
							<b>${{ $seatmodel->pivot->priceRangeMin }} - {{ ($seatmodel->pivot->priceRangeMax==0)?"Above":"$ ".$seatmodel->pivot->priceRangeMax }} *</b>
							<ul class="searchlist">
							@foreach($comp_priority->companylist->seatpackage as $seatpackage)
								@if($seatpackage->pivot->seatmodelID == 2)
									<li>{{$seatpackage->name}}</li>
								@endif
							@endforeach
							</ul>
							
						
						@endif
						
					@endforeach
					</td>
					
					<td>
					<ul class="searchlist">
					 @foreach ($comp_priority->companylist->location as $location) 
						<li>{{ $location->name }}</li>
					  @endforeach
					  </ul>
					</td>
					
					<td style="text-align:center;">
					<?php $imgsrc = ($comp_priority->companylist->aumanaged==1)?"aussiemanaged.png":"noaussiemanaged.png"; ?>
					<img src="<?php echo get_template_directory_uri(); ?>/images/<?php echo $imgsrc ?>"/>
					</td>
					
					<td>
					<ul class="searchlist">
						@foreach ($comp_priority->companylist->feature as $feature) 
						<li>{{ $feature->name }}</li>
					  @endforeach
					</ul>
					</td>
					
					</tr>
					
				@endforeach
				@if(count($prioritylist) != 0 && isset($_GET['page']) <= 1)
				<tr>
				<td colspan="7" style="border-left:0;border-right:0;text-align:center;padding:0">
				<!-- start of ads placement -->
				
				{{show_ads('search','mid',true)}}
				
				
				<!--end ads placement-->

				</td>
				</tr>
				
				@endif
				@foreach($search as $compInfo)
					<tr style="background-color:{{$bgcolor}};">
					<td><a href='{{URL::to("company/$compInfo->id")}}'>{{$compInfo->name}}</a></td>
					<td>
					<ul class="searchlist">
					 @foreach ($compInfo->services as $service) 
						<li>{{ $service->name }}</li>
					  @endforeach
					</ul>
					</td>
					<td id="tdseatmodel1{{$compInfo->id}}">
					@foreach($compInfo->seatmodel as $seatmodel)
					@if($seatmodel->id == 1)
						@if(isset($_GET['price']) && $seatmodel->pivot->priceRangeMin > max($_GET['price']))
								<script>
									$(document).ready(function(){
										$("#tdseatmodel1{{$compInfo->id}}").css("background-color","#ccc");
										//alert("asd");
									});
								</script>
							
							@endif
					
						<b>${{ $seatmodel->pivot->priceRangeMin }} - ${{ ($seatmodel->pivot->priceRangeMax==0)?"up":$seatmodel->pivot->priceRangeMax }}  *</b>
						<ul class="searchlist">
						@foreach($compInfo->seatpackage as $seatpackage)
							@if($seatpackage->pivot->seatmodelID == 1)
								<li>{{$seatpackage->name}}</li>
							@endif
						@endforeach
						</ul>
					@endif
					@endforeach
					</td>
					<td id="tdseatmodel{{$compInfo->id}}">
					@foreach($compInfo->seatmodel as $seatmodel)
						@if($seatmodel->id == 2)
							@if((isset($_GET['price']) && $seatmodel->pivot->priceRangeMin > max($_GET['price'])) ||
								(isset($_GET['priceadv']) && $seatmodel->pivot->priceRangeMin > max($_GET['priceadv'])))
								<script>
									$(document).ready(function(){
										$("#tdseatmodel{{$compInfo->id}}").css("background-color","#ccc");
										$("#tdseatmodel{{$compInfo->id}}").attr("title","Out of buget range");
										//alert("asd");
									});
								</script>
							
							@endif
							<b>${{ $seatmodel->pivot->priceRangeMin }} - {{ ($seatmodel->pivot->priceRangeMax==0)?"Above":"$ ".$seatmodel->pivot->priceRangeMax }} *</b>
							<ul class="searchlist">
							@foreach($compInfo->seatpackage as $seatpackage)
								@if($seatpackage->pivot->seatmodelID == 2)
									<li>{{$seatpackage->name}}</li>
								@endif
							@endforeach
							</ul>
							
						
						@endif
						
					@endforeach
					</td>
					
					<td>
					<ul class="searchlist">
					 @foreach ($compInfo->location as $location) 
						<li>{{ $location->name }}</li>
					  @endforeach
					  </ul>
					</td>
					<td style="text-align:center;">
					
					<?php $imgsrc = ($compInfo->aumanaged==1)?"aussiemanaged.png":"noaussiemanaged.png"; ?>
					<img src="<?php echo get_template_directory_uri(); ?>/images/<?php echo $imgsrc ?>"/>
					
					
					</td>
					
					<td>
					<ul class="searchlist">
						@foreach ($compInfo->feature as $feature) 
						<li>{{ $feature->name }}</li>
					  @endforeach
					</ul>
					</td>
					
					</tr>
					@if(($ctr%3)==0 && ($ctr1 <= $flag ))
					<tr><td colspan=7 style="border-left:0;border-right:0;text-align:center;padding:0">
					{{show_ads('search','mid',true)}}
					</td></tr>
					
					@endif
					
					<?php 
					$ctr++; 
					$ctr1++; 
					$bgcolor = ($bgcolor == "#fff")?"#f0f0f0":"#fff"; 
					
					?>
				@endforeach
				</tbody>
			</table>
			